//login_api.js
import service from './request'

/**
 * 添加网关路由服务
 */
export const login = data => {
    return service({
        url: '/login',
        method: 'post',
        data
    })
};


/**
 * 添加网关路由服务
 */
export const logout = data => {
    return service({
        url: '/logout',
        method: 'post',
        data
    })
};